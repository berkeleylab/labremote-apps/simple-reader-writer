This application is used to do a simple readout of an agilent PS as well as for setting currents and voltages.

Clone the repository
<pre>
git clone --recursive https://gitlab.cern.ch/berkeleylab/labremote-apps/simple-reader-writer.git
git submodule update --remote
</pre>

Note: labRemote is added as a submodule.

To compile:
- `mkdir build`
- `cd build`
- `cmake3 ../`
- `make`

To update the labRemote submodule:
  `git submodule update`

To push the latest submodule version:
- `git commit -m "Pulled down update to submodule_dir"`
- `git push origin master`

<details>
To create from an empty	directory
- create git repository
  `git clone https://gitlab.cern.ch/berkeleylab/labremote-apps/simple-reader-writer.git`
- add labRemote as a submodule:
  `git submodule add https://gitlab.cern.ch/berkeleylab/labRemote.git`
- commit and push changes to your git repository
</details>